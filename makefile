ASMFLAGS=-f elf64 -o


.PHONY: clean test

lib.o: lib.asm
	nasm $(ASMFLAGS) $@ $<

dict.o: lib.inc
	nasm $(ASMFLAGS) $@ $<

main.o: *.inc
	nasm $(ASMFLAGS) $@ $<

program: main.o lib.o dict.o
	ld -o $@ $^
 
clean:
	rm -rf ./*.o program

test:
	python3 test.py
