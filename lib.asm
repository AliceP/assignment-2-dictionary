section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy


 
; Принимает код возврата и завершает текущий процесс

exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
	pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, 0
    mov rsi, 1
    dec rsp
    mov byte [rsp], 0
    mov rax, rdi
    .loop:
        mov rdi, 10
        mov rdx, 0
        div rdi
        add rdx, '0'
        dec rsp
        inc rsi
        mov byte [rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx
    mov rax, 0
    mov rbx, 0
    .loop:
        mov al, byte [rdi]
        mov bl, byte [rsi]
        cmp al, bl
        jne .notequal
        cmp al, 0
        je .equal
        inc rdi
        inc rsi
        jmp .loop
    .notequal:
        pop rbx
        mov rax, 0
        ret
    .equal:
        pop rbx
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 8
	mov rsi, rsp
	xor rdi, rdi
    xor rax, rax
    mov rdx, 1
	syscall
	cmp rax, 0
	jle .end
	pop rax
	ret
.end:
	pop rax
	xor rax, rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13
    push r14
    push r15
    mov r13, rdi
    mov r14, rdi
    mov r15, rsi

    .skip_whitespace:
        call read_char
        cmp al, 0x20
        je .skip_whitespace
        cmp al, 0xA
        je .skip_whitespace
        cmp al, 0x9
        je .skip_whitespace

    .read:
        test rax,rax
        je .done
        cmp al, 0x20
        je .done
        cmp al, 0xA
        je .done
        cmp al, 0x9
        je .done
        dec r15
        test r15, r15
        jz .break
        mov byte[r14], al
        inc r14
        call read_char
        jmp .read

    .done:
        mov byte[r14],0
        sub r14, r13
        mov rdx, r14
        mov rax, r13
        jmp .exit
    .break:
        xor rax, rax
        jmp .exit

    .exit:
        pop r15
        pop r14
        pop r13
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        mov al, [rdi+rcx]
        test al, al 
        jz .exit
        sub al, "0"
        js .exit
        cmp al, 9
        ja .exit
        inc rcx
        imul rdx, 10
        add dl, al
        jmp .loop
    .exit:
        mov rax, rdx
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
	test rdx, rdx
	jz .end
    inc rdx
    neg rax
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    xor rcx, rcx
    pop rdx
    pop rsi
    pop rdi
    inc rax
    cmp rax, rdx
    jg .err

.loop:
    cmp rax, rcx
	je .end
	mov dl, byte [rdi + rcx]
	mov [rsi + rcx], dl
	inc rcx
    jmp .loop	

.err: 
    xor rax, rax
    ret

.end:
    ret    

