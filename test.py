import subprocess

inputs = ["third", "second", "first", "a"*300, "smth"]
outputs = ["third_string", "second_string", "first_string", "", ""]
errors = ["", "", "", "Input is too long", "Value not found"]


subprocess.call(['make', 'clean'])
subprocess.call(['make', 'program'])

for i in range (len(inputs)):
    s = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = s.communicate(input=inputs[i].encode())
    out = stdout.decode().strip()
    err = stderr.decode().strip()
    if (out == outputs[i] and err == errors[i]):
        print("test ", i + 1," :successful")
        if (out == ""):
            print("error: ", err)
        else:
            print("out: ", out)
    else:
        print(out)
        print(err)
        print("test ", i + 1, " :failed")
