%include "lib.inc"
%include "colon.inc"

section .text
global find_word

find_word:
	.loop:
		add rsi, 8
        push rdi
		call string_equals
        pop rdi
		test rax, rax
		jnz .success

		sub rsi, 8
		test rsi, rsi
		jz .end
		jmp .loop
	.success:
		mov rax, rsi
		ret
	.end:
		xor rax, rax
